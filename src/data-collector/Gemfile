# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.8'

gem 'database_cleaner', '1.5.3'
gem 'faye', '1.2.4'
gem 'jquery-rails', '4.3.1'
gem 'mongoid', '6.4.8'
gem 'rails-healthcheck', '1.0.3'
gem 'rake', '12.3.3'
gem 'sass-rails', '5.0.6'
gem 'thin', '1.7.0'

# Configure application
gem 'config', '2.2.1'

gem 'service-base', path: '../gems/service-base'

group :development, :test do
  gem 'factory_girl_rails', '~> 4.0'
  gem 'faker', '1.7.3'
  gem 'rails-controller-testing', '1.0.2'
  gem 'rspec-expectations', '~> 3.8.0'
  gem 'rspec-rails', '~> 3.8.0'

  # Call 'byebug' anywhere in the code to stop execution and get a
  # debugger console
  gem 'byebug', '11.0.1', platforms: %i[mri mingw x64_mingw]
  gem 'rubocop', '~> 0.74.0', require: false
  gem 'rubocop-rails', require: false
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %>
  # anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring', '2.0.2'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'simplecov', '0.14.1', require: false, group: :test
